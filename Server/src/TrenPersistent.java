
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.sql.*; 

import org.json.simple.JSONObject;

public class TrenPersistent {

	private Tren t;
	private ArrayList<Tren> ts= new ArrayList<Tren>();
	private ArrayList<String> trenu = null;
	private static final String bazaDate = "jdbc:mysql://localhost:3306/sneakers?autoReconnect=true&useSSL=false";
	private static final String queryStatiePlecare = "select * from trains where statiePlecare=" + "(?)";
	private static final String queryStatieSosire = "select * from trains where statieSosire=" + "(?)";
	private static final String queryDupaDurata = "select * from trains where durataMinute<" + "(?)";
	private static final String queryStatiePlecareSosire = "select * from trains where statiePlecare=" + "(?)" + "and statieSosire=" + "(?);";
	private static final String queryID = "select * from trains where id="+"(?);";
	private static final String insertTren = "INSERT INTO trains (id,statiePlecare,statieSosire,durataMinute,locuriDisponibile,pret)\r\n" + "VALUES\r\n" + "(?, ?, ?, ?, ?, ?);";
	private static final String lista = "select * from trains;";
	private static final String editTren = "UPDATE trains \r\n" + "SET \r\n" + "statiePlecare = ?,\r\n" + "statieSosire=?,\r\n"
			+ "durataMinute=?,\r\n" + "locuriDisponibile=?,\r\n" + "pret=?\r\n" + "WHERE\r\n" + "    id = ?;";
	private static final String deleteTren = "DELETE FROM trains \r\n" + "WHERE\r\n" + "id = ?;";
	private static final String updateLocuri = "UPDATE trains \r\n" + "SET \r\n" + "locuriDisponibile = locuriDisponibile - 1\r\n" + "WHERE\r\n" + "id = ?;";
	private static List<Calator> calatori = new ArrayList<>();
	
	public TrenPersistent()
	{
		
	}
	
	public void addObserver(Calator c) {
        TrenPersistent.calatori.add(c);
    }
 
    public void removeObserver(Calator c) {
        TrenPersistent.calatori.remove(c);
    }
 
    public void notificareCalatori(Tren t) {
        for (Calator c : TrenPersistent.calatori) {
            c.update(t);
        }
    }
	
	public JTable cautareTrenDupaStatiaDePlecare(String statiePlecare)  {
		 
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");  
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryStatiePlecare);
	    			fd.setString(1, statiePlecare);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public JTable cautareTrenDupaStatiaDeSosire(String statieSosire)  {
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");  
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryStatieSosire);
	    			fd.setString(1, statieSosire);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public JTable cautareTrenDupaDurata(int durata)  {
	
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryDupaDurata);
	    			fd.setInt(1, 60*durata);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public JTable intreStatii(String statiePlecare,String statieSosire)  {
		
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryStatiePlecareSosire);
	    			fd.setString(1, statiePlecare);
	    			fd.setString(2, statieSosire);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public JTable cautareTren(int id)  {
		
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryID);
	    			fd.setInt(1, id);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public Boolean gasitTren(int id)  {
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	ResultSet rs = null; 
        	
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(queryID);
    			fd.setInt(1, id);
    			rs = fd.executeQuery();
    			if(rs.next()) return true;
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
        	
        	}
        
            catch(Exception e)
        	{ System.out.println(e);}  
        	    
	return false;
	}
	
	public void salvareTren(Tren t) {
		BiletPersistent bilet = new BiletPersistent();
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(insertTren);
    			fd.setInt(1, t.getIdTren());
    			fd.setString(2, t.getStatiePlecare());
    			fd.setString(3, t.getStatieDestinatie());
    			fd.setInt(4, t.getDurataInMinute());
    			fd.setInt(5, t.getLocuriLibere());
    			fd.setFloat(6, t.getPret());
    			fd.executeUpdate();
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
    		
    		bilet.initializareObserveri();
    		
    		this.notificareCalatori(t);
    		
        	}
        
            catch(Exception e)
        	{ System.out.println(e);} 
	}
	
	public JTable listaTrenuri()  {
		 
		 DefaultTableModel model = new DefaultTableModel(); 
		 JTable table = new JTable(model); 

		    model.addColumn("Id-ul trenului");
	        model.addColumn("Statia de plecare");
	        model.addColumn("Statia de sosire");
	        model.addColumn("Durata in minute");
	        model.addColumn("Locuri disponibile");
	        model.addColumn("Pret");

	        try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");  
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(lista);
	    			rs = fd.executeQuery();
	    			while(rs.next())  
		        		model.addRow(new Object[]{rs.getString("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getString("durataMinute"),rs.getString("locuriDisponibile"),rs.getString("pret")});
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
		
		return table;
	}
	
	
	public void modificareTren(int id, String statiePlecare, String statieSosire,int durataMinute,int locuri,float pret)  {
		
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(editTren);
    			fd.setString(1, statiePlecare);
    			fd.setString(2, statieSosire);
    			fd.setInt(3, durataMinute);
    			fd.setInt(4, locuri);
    			fd.setFloat(5, pret);
    			fd.setInt(6, id);
    			fd.executeUpdate();
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
        	
        	}
        
            catch(Exception e)
        	{ System.out.println(e);} 
	}
	
	
	public void deleteTren(int id)  {
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(deleteTren);
    			fd.setInt(1, id);
    			fd.executeUpdate();
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
        	
        	}
        
            catch(Exception e)
        	{ System.out.println(e);} 
	}
	
	
	public float pretTren(int id)  {
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	ResultSet rs = null; 
        	
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(queryID);
    			fd.setInt(1, id);
    			rs = fd.executeQuery();
    			rs.next();
    			return rs.getFloat("pret");
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
        	
        	}
        
            catch(Exception e)
        	{ System.out.println(e);}  
        	    
		return 0.0f;
	}
	
	public void updateLocuri(int id)  {
		try{  
        	Class.forName("com.mysql.jdbc.Driver");  
        	Connection con=DriverManager.getConnection(  
        	bazaDate,"root","madalin");
        	
        	PreparedStatement fd = null;
    		try {
    			fd = con.prepareStatement(updateLocuri);
    			fd.setInt(1,id);
    			fd.executeUpdate();
    			
    		} catch (SQLException e) {
    			e.printStackTrace();
    		} finally {
    			try {
    				fd.close();
    				con.close();
    			} catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}

    		}
        	
        	}
        
            catch(Exception e)
        	{ System.out.println(e);}  
        	    
	}
	
	
	
	public String date(int id)  {
		
		 String data=null;
		 
		 try{  
	        	Class.forName("com.mysql.jdbc.Driver");  
	        	Connection con=DriverManager.getConnection(  
	        	bazaDate,"root","madalin");
	        	ResultSet rs = null; 
	        	
	        	PreparedStatement fd = null;
	    		try {
	    			fd = con.prepareStatement(queryID);
	    			fd.setInt(1, id);
	    			rs = fd.executeQuery();
	    			rs.next();
	    			return rs.getString("statiePlecare")+" "+rs.getString("statieSosire")+" "+rs.getString("pret");
	    			
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		} finally {
	    			try {
	    				fd.close();
	    				con.close();
	    			} catch (SQLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    		}
	        	
	        	}
	        
	            catch(Exception e)
	        	{ System.out.println(e);}  
	        	    
			
		return data;
		
	}
		
	public void conversieFormate()  {
		 
	         XMLEncoder encoder = null;
	         FileWriter writer = null;
			try {
				writer = new FileWriter("./trains.csv");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	         JSONObject obj = new JSONObject();
	
	         try{  
		        	Class.forName("com.mysql.jdbc.Driver");  
		        	Connection con=DriverManager.getConnection(  
		        	bazaDate,"root","madalin");  
		        	ResultSet rs = null; 
		        	
		        	PreparedStatement fd = null;
		    		try {
		    			fd = con.prepareStatement(lista);
		    			rs = fd.executeQuery();
		    			while(rs.next())  {
		    			
			        		 obj.put("Id-ul trenului", rs.getString("id"));
					         obj.put("Statia de plecare", rs.getString("statiePlecare"));
					         obj.put("Statia de sosire", rs.getString("statieSosire"));
					         obj.put("Durata in minute", rs.getString("durataMinute"));
					         obj.put("Locuri disponibile", rs.getString("locuriDisponibile"));
					         obj.put("Pret", rs.getString("pret"));
					         
					         try (FileWriter file = new FileWriter("./trains.json", true)) {

					             file.write(obj.toJSONString());
					             file.flush();

					         } catch (IOException e) {
					             e.printStackTrace();
					         }
				        	 
					         trenu = new ArrayList<String>();
					         
					         trenu.add(rs.getString("id"));
					         trenu.add(rs.getString("statiePlecare"));
					         trenu.add(rs.getString("statieSosire"));
					         trenu.add(rs.getString("durataMinute"));
					         trenu.add(rs.getString("locuriDisponibile"));

				             CSVUtils.writeLine(writer, trenu);
					 
				        	t=new Tren(rs.getInt("id"),rs.getString("statiePlecare"),rs.getString("statieSosire"),rs.getInt("durataMinute"),rs.getInt("locuriDisponibile"),rs.getFloat("pret"));
				        	     
				        		 
				        	ts.add(t);
				        		
				        	     
				         }
				         
				         try {
			    			 FileOutputStream fos = new FileOutputStream(new File("./trains.xml"));
			    			 encoder = new XMLEncoder(fos);
			    			 encoder.writeObject(ts);
			    			 encoder.close();
			    			 
			    		 }
			    		 
			    		 catch(IOException ex) {
			    			 ex.printStackTrace();
			    		 }
				         
				         writer.flush();
				         writer.close();      
			        		
		    		} catch (SQLException e) {
		    			e.printStackTrace();
		    		} finally {
		    			try {
		    				fd.close();
		    				con.close();
		    			} catch (SQLException e) {
		    				// TODO Auto-generated catch block
		    				e.printStackTrace();
		    			}

		    		}
		        	
		        	}
		        
		            catch(Exception e)
		        	{ System.out.println(e);}  

	}

}
