public class Tren {
	private int idTren;
	private String statiePlecare;
	private String statieDestinatie;
	private int durataInMinute;
	private int locuriLibere;
	private float pret;
	
	public Tren() {
		
	}
	
	public Tren(int a,String b,String c,int d,int f,float p) {
		this.idTren=a;
		this.statiePlecare=b;
		this.statieDestinatie=c;
		this.durataInMinute=d;
		this.locuriLibere=f;
		this.pret=p;
	}
	
	public String getStatiePlecare() {
		return statiePlecare;
	}
	public void setStatiePlecare(String statiePlecare) {
		this.statiePlecare = statiePlecare;
	}
	public String getStatieDestinatie() {
		return statieDestinatie;
	}
	public void setStatieDestinatie(String statieDestinatie) {
		this.statieDestinatie = statieDestinatie;
	}
	public int getDurataInMinute() {
		return durataInMinute;
	}
	public void setDurataInMinute(int durataInMinute) {
		this.durataInMinute = durataInMinute;
	}
	public int getIdTren() {
		return idTren;
	}
	public void setIdTren(int idTren) {
		this.idTren = idTren;
	}
	public int getLocuriLibere() {
		return locuriLibere;
	}
	public void setLocuriLibere(int locuriLibere) {
		this.locuriLibere = locuriLibere;
	}
	
	public String toString() {
		return this.idTren+" "+this.statiePlecare+" "+this.statieDestinatie+" "+this.durataInMinute+" "+this.locuriLibere+" "+this.pret+"\n";
	}

	public float getPret() {
		return pret;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}
	
	public static class Builder {
		private int idTren;
		private String statiePlecare;
		private String statieDestinatie;
		private int durataInMinute;
		private int locuriLibere;
		private float pret;
		
		public Builder id(int a) {
			this.idTren=a;
			return this;
		}
		
		public Builder plecare(String a)
		{
			this.statiePlecare=a;
			return this;
		}
		
		public Builder sosire(String a)
		{
			this.statieDestinatie=a;
			return this;
		}
		
		public Builder durata(int a)
		{
			this.durataInMinute=a;
			return this;
		}
		
		public Builder locuri(int a)
		{
			this.locuriLibere=a;
			return this;
		}
		
		public Builder pret(float a) {
			this.pret=a;
			return this;
		}
		
		public Tren build() {
			Tren t = new Tren();
		    t.idTren=this.idTren;
		    t.statiePlecare=this.statiePlecare;
		    t.statieDestinatie=this.statieDestinatie;
		    t.locuriLibere=this.locuriLibere;
		    t.durataInMinute=this.durataInMinute;
		    t.pret=this.pret;
		    return t;
		}
		
	}

}

